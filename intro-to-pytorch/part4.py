import torch
from torchvision import  datasets, transforms
import helper
import matplotlib.pyplot as plt

from torch import nn
import torch.nn.functional as F
from torch import optim
#define a transform to norm the data
transform=transforms.Compose([transforms.ToTensor(),transforms.Normalize((0.5,),(0.5,))])
#load data
trainset=datasets.FashionMNIST('./pytorch/F_MNIST_data/',download=True,train=True,transform=transform)
#this initialize method get the training data
valset=datasets.FashionMNIST('./pytorch/F_MNIST_data/',download=True,train=False,transform=transform)

train_loader=torch.utils.data.DataLoader(trainset,batch_size=64,shuffle=True)
val_loader=torch.utils.data.DataLoader(valset,batch_size=64,shuffle=True)
print(val_loader)

#image,label=next(iter(train_loader))
#helper.imshow(image[0,:]);

#plt.show()
#set model
# model=nn.Sequential(nn.Linear(784,128),
#                     nn.ReLU(),
#                     nn.Linear(128,64),
#                     nn.ReLU(),
#                     nn.Linear(64,10),
#                     nn.LogSoftmax(dim=1))
# #calculate loss functiontpu
# criteria=nn.CrossEntropyLoss()
# optimizer=optim.SGD(model.parameters(),lr=0.6)
# # trainging
# epoch=10
# for i in range(epoch):
#     running_loss = 0
#     for image,label in train_loader:
#         #flatten image to a vector
#         vecto_image=image.view(image.shape[0],-1)
#         #set optimizer=0
#         optimizer.zero_grad()
#         #calculate the output
#         output=model(vecto_image)
#         #find the loss
#         loss=criteria(output,label)
#         loss.backward()
#         optimizer.step()
#         running_loss+=loss.item()
#     else:
#         print(f"Training loss: {running_loss / len(train_loader)}")
#
# image,label=next(iter(val_loader))
# image=image.view(image.shape[0],-1)
# with torch.no_grad():
#     output=model(image)
# ps=torch.exp(output)
# helper.view_classify(image.view(1, 28, 28), ps)
# plt.show()
class Classification(nn.Module):
    def __init__(self):
        super().__init__()
        self.fc1=nn.Linear(784,128)
        self.fc2=nn.Linear(128,64)
        self.fc3=nn.Linear(64,10)
    def forward(self,x):
        x=x.view(x.shape[0],-1)
        x=nn.functional.relu(self.fc1(x))
        x=nn.functional.relu(self.fc2(x))
        x=nn.functional.log_softmax(self.fc3(x),dim=1)
        return x

model=Classification()
criteria=nn.CrossEntropyLoss()
optimizer=optim.Adam(model.parameters(), lr=0.003)

for i in range(5):
    running_loss=0
    for image,label in train_loader:
        output=model.forward(image)
        loss=criteria(output,label)
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()
        running_loss+=loss.item()
    else:
        print("Loss",running_loss)


dataiter = iter(val_loader)
images, labels = dataiter.next()
img = images[1]

# TODO: Calculate the class probabilities (softmax) for img
ps = torch.exp(model.forward(img))

# Plot the image and probabilities
helper.view_classify(img, ps, version='Fashion')
plt.show()