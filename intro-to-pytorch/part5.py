import torch
from torch import nn
from torchvision import datasets,transforms
import torch.nn.functional as F
transform = transforms.Compose([transforms.ToTensor(),
                                transforms.Normalize((0.5,),(0.5,))])
trainset=datasets.FashionMNIST('~/.pytorch/F_MNIST_data/',train=True,transform=transform,download=True,)
#build the trainloader
trainloader=torch.utils.data.DataLoader(trainset,batch_size=64,shuffle=True)
#building the testing set
testingset=datasets.FashionMNIST('~/pytorch/F_MNIST_data/',train=False,transform=transform,download=True)
testloader=torch.utils.data.DataLoader(testingset,batch_size=64,shuffle=True)


class Classifier(nn.Module):
    def __init__(self):
        self.fc1=nn.Linear(784,128)
        self.fc2=nn.Linear(128,64)
        self.fc3=nn.Linear(64,10)
    def forward(self,x):
        x=x.view(x.shape[0],-1)
        x=F.relu(self.fc1(x))
        x=F.relu(self.fc2(x))
        x=F.log_softmax(self.fc3(x),dim=1)
        return x

model=Classifier()
img,label=next(iter(testloader))
ps=torch.exp(model(img))


